package mapandin.yonathan.uasSpatu

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_spatu.*

class AdapterDataSpatu(val dataSpatu : List<HashMap<String,String>>,
                       val spatuActivity : SpatuActivity) :
    RecyclerView.Adapter<AdapterDataSpatu.HolderDataSpatu>(){

    var var2 : String = ""

    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataSpatu.HolderDataSpatu {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_spatu,p0,false)
        return HolderDataSpatu(v)
    }

    override fun getItemCount(): Int {
        return dataSpatu.size
    }

    override fun onBindViewHolder(p0: AdapterDataSpatu.HolderDataSpatu, p1: Int) {
        val data = dataSpatu.get(p1)
        p0.txIdSpatu.setText(data.get("id_spatu"))
        p0.txSpatu.setText(data.get("nama_spatu"))
        p0.txHarga.setText(data.get("harga"))
        p0.txJenisSpatu.setText(data.get("gender_spatu"))
        p0.txMerk.setText(data.get("nama_merk"))

        if(p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = spatuActivity.daftarMerk.indexOf(data.get("nama_merk"))
            spatuActivity.spinMerk.setSelection(pos)
            spatuActivity.edIdSpatu.setText(data.get("id_spatu"))
            spatuActivity.edNamaMerk.setText(data.get("nama_spatu"))
            spatuActivity.edHarga.setText(data.get("harga"))
            var2 = data.get("gender_spatu").toString()
            if(var2 == "Wanita"){
                spatuActivity.rg.check(R.id.rbPria)
            }
            else if(var2 == "Wanita"){
                spatuActivity.rg.check(R.id.rbWanita)
            }
            Picasso.get().load(data.get("url")).into(spatuActivity.imUpload);
        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    inner class HolderDataSpatu(v : View) : RecyclerView.ViewHolder(v){
        val txIdSpatu = v.findViewById<TextView>(R.id.txIdSpatu)
        val txSpatu = v.findViewById<TextView>(R.id.txSpatu)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
        val txJenisSpatu = v.findViewById<TextView>(R.id.txJenisSpatu)
        val txMerk = v.findViewById<TextView>(R.id.txMerk)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
    }
}