package mapandin.yonathan.uasSpatu

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDaftarSpatu ->{
                val intent = Intent(this, SpatuActivity::class.java)
                startActivity(intent)
            }
            R.id.btnDataMerkSpatu ->{
                val intent = Intent(this, MerkActivity::class.java)
                startActivity(intent)
            }
            R.id.btnRewvid ->{
                val intent = Intent(this, SpatuReview::class.java)
                startActivity(intent)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnDaftarSpatu.setOnClickListener(this)
        btnDataMerkSpatu.setOnClickListener(this)
        btnRewvid.setOnClickListener(this)
    }
}
