package mapandin.yonathan.uasSpatu

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {
    companion object{
        val DB_Name = "media"
        val DB_Ver = 1
    }


    override fun onCreate(db: SQLiteDatabase?) {
        val tVideo= "create table video(id_video text primary key, id_cover text not null, video_title text not null)"
        val insVideo= "insert into video(id_video,id_cover,video_title) values " +
                "('0x7f0d0003','0x7f0d0000','video1')," +
                "('0x7f0d0004','0x7f0d0001','video2')," +
                "('0x7f0d0005','0x7f0d0002','video3')"
        db?.execSQL(tVideo)
        db?.execSQL(insVideo)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}

//0x7f0d0003

//.field public static final video2:I = 0x7f0d0004

//.field public static final video3:I = 0x7f0d0005