package mapandin.yonathan.uasSpatu

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_merk.*

class AdapterDataMerk(val dataMerk : List<HashMap<String,String>>,
                      val merkActivity : MerkActivity) :
    RecyclerView.Adapter<AdapterDataMerk.HolderDataProdi>(){
    override fun onCreateViewHolder(
        p0: ViewGroup,
        p1: Int
    ): AdapterDataMerk.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_merk,p0,false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataMerk.size
    }

    override fun onBindViewHolder(p0: AdapterDataMerk.HolderDataProdi, p1: Int) {
        val data = dataMerk.get(p1)
        p0.txIdMerk.setText(data.get("id_merk"))
        p0.txNamaMerk.setText(data.get("nama_merk"))

        if (p1.rem(2) == 0) p0.cLayout2.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout2.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout2.setOnClickListener(View.OnClickListener {
            merkActivity.edNamaMerk.setText(data.get("nama_merk"))
            merkActivity.id_merk = data.get("id_merk")
        })

    }

    inner class HolderDataProdi(v : View) : RecyclerView.ViewHolder(v){
        val txIdMerk = v.findViewById<TextView>(R.id.txIdMerk)
        val txNamaMerk = v.findViewById<TextView>(R.id.txNamaMerk)
        val cLayout2 = v.findViewById<ConstraintLayout>(R.id.cLayout2)

    }

}