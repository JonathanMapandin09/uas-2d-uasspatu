package mapandin.yonathan.uasSpatu

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_spatu.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class SpatuActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind ->{
                showDataSpatu(edNamaMerk.text.toString().trim())
            }
        }
    }

    lateinit var mediaHelper : MediaHelper
    lateinit var spatuAdapter : AdapterDataSpatu
    lateinit var merkAdapter : ArrayAdapter<String>
    var daftarSpatu = mutableListOf<HashMap<String,String>>()
    var daftarMerk = mutableListOf<String>()
    val url = "http://192.168.43.24/uasSpatu/show_data.php"
    val url2 = "http://192.168.43.24/uasSpatu/get_nama_merk.php"
    val url3 = "http://192.168.43.24/uasSpatu/query_upd_del_ins.php"
    var imStr = ""
    var pilihMerk = ""
    var var1 : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spatu)

        rg.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.rbPria -> var1 = "Pria"
                R.id.rbWanita -> var1 = "Wanita"
            }
        }


        spatuAdapter = AdapterDataSpatu(daftarSpatu, this)
        mediaHelper = MediaHelper(this)
        listSpatu.layoutManager = LinearLayoutManager(this)
        listSpatu.adapter = spatuAdapter

        merkAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,daftarMerk)
        spinMerk.adapter = merkAdapter
        spinMerk.onItemSelectedListener = itemSelected

        imUpload.setOnClickListener(this)
//        listMhs.addOnItemTouchListener(itemtouch)

        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataSpatu("")
        getNamaProdi()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinMerk.setSelection(0)
            pilihMerk = daftarMerk.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMerk = daftarMerk.get(position)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data, imUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    imUpload.setImageResource(R.color.colorHolo)
                    edIdSpatu.setText("")
                    edNamaMerk.setText("")
                    edHarga.setText("")
                    rg.clearCheck()
                    spinMerk.setSelection(0)
                    pilihMerk = daftarMerk.get(0)
                    showDataSpatu("")
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                    Date()
                )+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_spatu",edIdSpatu.text.toString())
                        hm.put("nama_spatu",edNamaMerk.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("jenis_spatu",var1)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_merk",pilihMerk)
                    }
                    "update" ->{
                        hm.put("mode","insert")
                        hm.put("id_spatu",edIdSpatu.text.toString())
                        hm.put("nama_spatu",edNamaMerk.text.toString())
                        hm.put("harga",edHarga.text.toString())
                        hm.put("jenis_spatu",var1)
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_merk",pilihMerk)
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_spatu",edIdSpatu.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarMerk.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMerk.add(jsonObject.getString("nama_merk"))
                }
                merkAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  }
        )

        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataSpatu(namaSpatu : String){
        val request = object : StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarSpatu.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var spatu = HashMap<String,String>()
                    spatu.put("id_spatu",jsonObject.getString("id_spatu"))
                    spatu.put("nama_spatu",jsonObject.getString("nama_spatu"))
                    spatu.put("harga",jsonObject.getString("harga"))
                    spatu.put("gender_spatu",jsonObject.getString("gender_spatu"))
                    spatu.put("nama_merk",jsonObject.getString("nama_merk"))
                    spatu.put("url",jsonObject.getString("url"))
                    daftarSpatu.add(spatu)
                }
                spatuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_spatu",namaSpatu)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}