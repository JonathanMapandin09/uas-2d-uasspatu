package mapandin.yonathan.uasSpatu

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_merk.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class MerkActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertP ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdateP ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDeleteP ->{
                queryInsertUpdateDelete("delete")
            }
        }

    }

    lateinit var merkAdapter : AdapterDataMerk
    var daftarMerk = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.24/uasSpatu/get_nama_merk.php"
    val url2 = "http://192.168.43.24/uasSpatu/query_upd_del_ins_merk.php"
    var id_merk : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merk)
        merkAdapter = AdapterDataMerk(daftarMerk, this)
        listMerk.layoutManager = LinearLayoutManager(this)
        listMerk.adapter = merkAdapter

        btnInsertP.setOnClickListener(this)
        btnUpdateP.setOnClickListener(this)
        btnDeleteP.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMerk()
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    edNamaMerk.setText("")
                    showDataMerk()
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nama_merk",edNamaMerk.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_merk",id_merk.toString())
                        hm.put("nama_merk",edNamaMerk.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_merk",id_merk.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMerk(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMerk.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var merk = HashMap<String,String>()
                    merk.put("id_merk",jsonObject.getString("id_merk"))
                    merk.put("nama_merk",jsonObject.getString("nama_merk"))
                    daftarMerk.add(merk)
                }
                merkAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}